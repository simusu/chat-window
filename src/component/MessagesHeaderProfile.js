import React, {Component} from 'react';


class MessagesHeaderProfile extends Component {
    render() {
        return (

            <div className="contact-profile">
                <img src="/images/thumb-01.jpg" alt=""/>
                <p>Charlie Puth
                    <span>Online</span>
                </p>
                <div className="media">
                    <i className="fa fa-phone" aria-hidden="true"></i>
                    <i className="fa fa-camera" aria-hidden="true"></i>
                </div>
            </div>

        );
    }
}

export default MessagesHeaderProfile;

