import React, {Component} from 'react';


class ChatHistory extends Component {
    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    };

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }
    render() {
        return (
            <div className="messages">
                {
                    (this.props.items && <ul>
                        {this.props.items.map((item, key) =>
                            <li className={item.type} key={key}>
                                <img src={item.profileImg}/>
                                <p>{item.message}</p>
                            </li>)
                        }
                    </ul>)
                }
                <div style={{ float:"left", clear: "both" }}
                     ref={(el) => { this.messagesEnd = el; }}>
                </div>
            </div>

        );
    }
}

export default ChatHistory;

