import React, {Component} from 'react';


class MessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event) => {
        this.setState({text: event.target.value});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.onSelectMessage(this.state.text);
        this.setState({text: ''});
    };

    render() {
        return (

            <div className="message-input">
                <div className="wrap">
                    <form className="App" onSubmit={this.handleSubmit}>
                        <input
                            type="text"
                            placeholder="Type Something"
                            value={this.state.text}
                            onChange={this.handleChange}
                            onSubmit={this.handleSubmit}
                        />
                        <button className="submit"><i className="fa fa-paper-plane" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>

        );
    }
}

export default MessageInput;

