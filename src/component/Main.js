import React, {Component} from 'react';
import MessagesHeaderProfile from './MessagesHeaderProfile'
import ChatHistory from './ChatHistory'
import MessageInput from './MessageInput'

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: {},
            items: [

                {
                    profileImg: '/images/thumb-01.jpg',
                    message: 'Lorem ipsum is a pseudo text used in web desig',
                    type: 'others'
                },
                {
                    profileImg: '/images/thumb-02.jpg',
                    message: 'React is a popular front-end JavaScript library',
                    type: 'owns'
                },
                {
                    profileImg: '/images/thumb-01.jpg',
                    message: 'No, I told him that.',
                    type: 'others'
                },
                {profileImg: '/images/thumb-02.jpg', message: 'Yeah, I told him that.', type: 'owns'},
                {
                    profileImg: '/images/thumb-01.jpg',
                    message: 'Many developers like because of its emphasis on writing reusable and modular UI components.',
                    type: 'others'
                },
            ]
        };
    }

    handleMessages = (text) => {
        const lastitem = this.state.items[this.state.items.length - 1];
        if (lastitem.type === 'owns') {
            this.state.items.push({
                profileImg: '/images/thumb-01.jpg',
                message: text,
                type: 'others'
            });
        } else {
            this.state.items.push({
                profileImg: '/images/thumb-02.jpg',
                message: text,
                type: 'owns'
            });
        }

        this.setState(this.state.items);
    }

    render() {
        return (
            <div className="frame">
                <div className="content">
                    <MessagesHeaderProfile/>
                    <ChatHistory items={this.state.items}/>
                    <MessageInput onSelectMessage={this.handleMessages}
                    />
                </div>
            </div>
        );
    }
}

export default Main;

